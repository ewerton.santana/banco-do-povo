Scenario: Realizar um depósito numa conta nova

Given abri uma conta corrente
When realizo um depósito de 300 reais
Then o saldo da minha conta será de 300 reais

Scenario: Realizar dois depósitos numa conta nova

Given abri uma conta corrente
When realizo um depósito de 300 reais
And realizo um depósito de 200 reais
Then o saldo da minha conta será de 500 reais