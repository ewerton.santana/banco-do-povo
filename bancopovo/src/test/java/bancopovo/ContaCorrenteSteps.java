package bancopovo;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.junit.Assert;

import br.ucsal.bes20182.testequalidade.bancopovo.ContaCorrente;

public class ContaCorrenteSteps extends Steps {
	ContaCorrente contaCorrenteUm;
	
	@Given("abri uma conta corrente")
	public void contaCorrente(){
		contaCorrenteUm = new ContaCorrente();
	}
	
	@When("realizo um depósito de $valor reais")
	public void depositoDeConta(Double valor){
		contaCorrenteUm.depositar(valor);
	}
	
	@Then("o saldo da minha conta será de $saldo reais")
	public void consultarSaldo(Double saldo){
		Assert.assertEquals(saldo, contaCorrenteUm.consultarSaldo());
	}
}
